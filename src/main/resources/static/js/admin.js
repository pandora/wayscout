$(document).ready(function() {

    var admin_type = $('#admin_type').attr('value');

    // Prevent AJAX Caching
    $.ajaxSetup({ cache: false });

    // Populate table rows
    render_hash_list(api_url(admin_type), admin_type + '_row.html');

    // Set ID in Delete Modal
    $(document).on("click", ".view-admin", function() {
        var id = $(this).data('id');

        $("#edit_data_id")  .val(id);
        $("#delete_data_id").val(id);

        populate_edit_modal(id, admin_type);
    });

    // Confirm Delete
    $('#btn_delete').click(function() {
        var id = $("#delete_data_id").val();
        api_call(api_url(admin_type) + id, 'DELETE');
    });

    $('#btn_add').click(function(){
        var payload = {};

        $(".form-contents .modal-body .form-group .form-control").each(function() {
            var field = $(this).attr('name');
            var value = massage_params(field, $(this).val());

            payload[field] = value;
        });

        $.ajax({
            url: api_url(admin_type), type: 'POST', dataType: "json",
            data: JSON.stringify(payload),
            contentType: "application/json",
            success: function (data) {

            },
            error: function (xhr, status, error) {
               
            }
        });
    });

    // Confirm edit
    $('#btn_edit').click(function(){
        var payload = {};

        $(".edit-contents .modal-body .form-group .form-control").each(function() {
            var field = $(this).attr('name');
            var value = massage_params(field, $(this).val());

            payload[field] = value;
        });

        $.ajax({
            url: api_url(admin_type) + $("#edit_data_id").val(), type: 'PUT', dataType: "json",
            data: JSON.stringify(payload),
            contentType: "application/json",
            success: function (data) {

            },
            error: function (xhr, status, error) {

            }
        });
    });

});