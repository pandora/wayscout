function api_url(admin_type) {
    return '/api/' + admin_type + '/';
}

function massage_params(field, value) {
    if ( (field == 'riders') || (field == 'parties') ) {
        return [ parseInt(value) ]
    }

    return value;
}

function render_hash_list(relative_url, template) {
    $.ajax({
        url: relative_url, type: "GET", dataType: "json",
        success: function (data) {
            $.get('/mustache/' + template, function (tmpl) {
                $.each(data, function() {
                    $('#details').append(Mustache.render(tmpl, { d: this }));
                });
            });
        },
        error: function (xhr, status, error) {

        }
    });
}

function api_call(relative_url, http_method) {

    $.ajax({
        url: relative_url, type: http_method, dataType: "json",
        success: function (data) {

        },
        error: function (xhr, status, error) {

        }
    });
}

function populate_edit_modal(id, admin_type) {

        $.ajax({
            url: api_url(admin_type) + id, type: "GET", dataType: "json",
            success: function (data) {
                $(".edit-contents .modal-body").each(function(index) {
                    $(this).find(".form-group").each(function(index) {
                        var input_field = $(this).find("input");
                        var field_name  = input_field.attr("name");

                        // Set each field in modal
                        input_field.val(data[field_name]);
                    });
                });
            },
            error: function (xhr, status, error) {

            }
        });
}
