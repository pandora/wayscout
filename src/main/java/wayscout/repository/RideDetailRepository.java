package wayscout.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.CrudRepository;
import wayscout.model.RideDetail;

import java.util.Optional;

public interface RideDetailRepository extends CrudRepository<RideDetail, Long> {

    @Query("SELECT rd FROM RideDetail rd where rd.riderId = :riderId AND rd.rideId = :rideId")
    Optional<RideDetail> findByRiderAndRide(@Param("riderId") Long riderId, @Param("rideId") Long rideId);

}

