package wayscout.repository;

import org.springframework.data.repository.CrudRepository;

import wayscout.model.Ride;

public interface RideRepository extends CrudRepository<Ride, Long> {

}