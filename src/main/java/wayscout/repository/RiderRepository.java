package wayscout.repository;

import org.springframework.data.repository.CrudRepository;

import wayscout.model.Rider;

public interface RiderRepository extends CrudRepository<Rider, Long> {

}
