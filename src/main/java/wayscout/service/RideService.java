package wayscout.service;

import java.util.ArrayList;
import java.util.List;

import wayscout.model.Ride;
import wayscout.repository.RideRepository;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("rideService")
public class RideService {

    @Autowired
    private RideRepository rideRepository;

    public Ride findById(Long id) {
        return rideRepository.findById(id).get();
    }

    public void saveRide(Ride ride) {
        rideRepository.save(ride);
    }

    public void updateRide(Ride ride){
        saveRide(ride);
    }

    public void deleteRideById(Long id){
        rideRepository.delete( findById(id) );
    }

    public void deleteAllRides(){
        rideRepository.deleteAll();
    }

    public List<Ride> findAllRides() {
        List<Ride> rides = new ArrayList<>();
        rideRepository.findAll().forEach(rides::add);

        return rides;
    }

}
