package wayscout.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wayscout.model.Party;
import wayscout.repository.PartyRepository;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service("partyService")
public class PartyService {

    @Autowired
    private PartyRepository partyRepository;

    public Party findById(Long id) {
        return partyRepository.findById(id).get();
    }

    public void save(Party party) {
        partyRepository.save(party);
    }

    public void update(Party party){
        save(party);
    }

    public void deleteById(Long id){
        partyRepository.delete( findById(id) );
    }

    public void deleteAll(){
        partyRepository.deleteAll();
    }

    public List<Party> findAll() {
        List<Party> parties = new ArrayList<>();
        partyRepository.findAll().forEach(parties::add);

        return parties;
    }

}

