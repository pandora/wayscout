package wayscout.service;

import java.util.ArrayList;
import java.util.List;

import wayscout.model.RideDetail;
import wayscout.repository.RideDetailRepository;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("rideDetailService")
public class RideDetailService {

    @Autowired
    private RideDetailRepository rideDetailRepository;

    public RideDetail findById(Long id) {
        return rideDetailRepository.findById(id).get();
    }

    public RideDetail findByRiderAndRide(Long rider_id, Long ride_id) {
        return rideDetailRepository.findByRiderAndRide(rider_id, ride_id).get();
    }

    public void saveRideDetail(RideDetail rideDetail) {
        rideDetailRepository.save(rideDetail);
    }

    public void updateRideDetail(RideDetail rideDetail){
        saveRideDetail(rideDetail);
    }

    public void deleteRideDetailById(Long id){
        rideDetailRepository.delete( findById(id) );
    }

    public void deleteAllRideDetails(){
        rideDetailRepository.deleteAll();
    }

    public List<RideDetail> findAllRideDetails() {
        List<RideDetail> rideDetails = new ArrayList<>();
        rideDetailRepository.findAll().forEach(rideDetails::add);

        return rideDetails;
    }

}