package wayscout.service;

import java.util.ArrayList;
import java.util.List;

import wayscout.model.Rider;
import wayscout.repository.RiderRepository;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("riderService")
public class RiderService {

    @Autowired
    private RiderRepository riderRepository;

    public Rider findById(Long id) {
        return riderRepository.findById(id).get();
    }

    public void saveRider(Rider rider) {
        riderRepository.save(rider);
    }

    public void updateRider(Rider rider){
        saveRider(rider);
    }

    public void deleteRiderById(Long id){
        riderRepository.delete( findById(id) );
    }

    public void deleteAllRiders(){
        riderRepository.deleteAll();
    }

    public List<Rider> findAllRiders() {
        List<Rider> riders = new ArrayList<>();
        riderRepository.findAll().forEach(riders::add);

        return riders;
    }

}
