package wayscout.controller;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import wayscout.model.Party;
import wayscout.model.Rider;
import wayscout.service.PartyService;
import wayscout.service.RiderService;

@RestController
@RequestMapping("/api")
class PartyController {

    @Autowired
    protected PartyService partyService;

    @Autowired
    RiderService riderService;

    public static final Logger logger = LoggerFactory.getLogger(PartyController.class);

    private HttpHeaders headers = new HttpHeaders();

    @RequestMapping(value = "/party/", method = RequestMethod.GET)
    public ResponseEntity<List<Party>> listAll() {
        List<Party> all = (List<Party>) partyService.findAll();

        if(all.isEmpty()) return new ResponseEntity(HttpStatus.NO_CONTENT);

        return new ResponseEntity<List<Party>>(all, HttpStatus.OK);
    }

    @RequestMapping(value = "/party/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getParty(@PathVariable("id") Long id) {
        Party party = partyService.findById(id);

        return party == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : new ResponseEntity<Party>(party, HttpStatus.OK);
    }

    @RequestMapping(value = "/party/", method = RequestMethod.POST)
    public ResponseEntity<?> createParty(@RequestBody Party party, UriComponentsBuilder ucBuilder) {
        Set<Rider> riders = new HashSet<>();

        for (Object rider : party.getRiders()) {
            Rider currentRider = riderService.findById(new Long((int) rider));
            riders.add(currentRider);
        }

        party.setRiders(riders);
        partyService.save(party);

        headers.setLocation(ucBuilder.path("/party/").buildAndExpand().toUri());
        return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/party/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateParty(@PathVariable("id") Long id, @RequestBody Party party) {
        Set<Rider> riders = new HashSet<>();

        Party currentParty = partyService.findById(id);

        for (Object rider : party.getRiders()) {
            Rider currentRider = riderService.findById(new Long((int) rider));
            riders.add(currentRider);
        }

        currentParty.setRiders(riders);
        currentParty.setName(party.getName());

        partyService.update(currentParty);
        return currentParty == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : new ResponseEntity<Party>(currentParty, HttpStatus.OK);
    }

    @RequestMapping(value = "/party/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteParty(@PathVariable("id") Long id) {
        Party party = partyService.findById(id);
        partyService.deleteById(id);

        return new ResponseEntity<Party>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/party/", method = RequestMethod.DELETE)
    public ResponseEntity<Party> deleteAllParties() {
        partyService.deleteAll();
        return new ResponseEntity<Party>(HttpStatus.NO_CONTENT);
    }

}
