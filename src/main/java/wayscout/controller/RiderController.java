package wayscout.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import wayscout.model.Rider;
import wayscout.service.RiderService;

@RestController
@RequestMapping("/api")
public class RiderController {

    public static final Logger logger = LoggerFactory.getLogger(RiderController.class);

    @Autowired
    RiderService riderService;

    @RequestMapping(value = "/rider/", method = RequestMethod.GET)
    public ResponseEntity<List<Rider>> listAllRiders() {
        List<Rider> riders = riderService.findAllRiders();

        return riders.isEmpty() ? new ResponseEntity(HttpStatus.NO_CONTENT) : new ResponseEntity<List<Rider>>(riders, HttpStatus.OK);
    }

    @RequestMapping(value = "/rider/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getRider(@PathVariable("id") Long id) {
        Rider rider = riderService.findById(id);

        return rider == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : new ResponseEntity<Rider>(rider, HttpStatus.OK);
    }

    @RequestMapping(value = "/rider/", method = RequestMethod.POST)
    public ResponseEntity<?> createRider(@RequestBody Rider rider, UriComponentsBuilder ucBuilder) {
        riderService.saveRider(rider);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/rider/").buildAndExpand().toUri());
        return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/rider/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateRider(@PathVariable("id") Long id, @RequestBody Rider rider) {
        Rider currentRider = riderService.findById(id);

        currentRider.setFirstName(rider.getFirstName());
        currentRider.setLastName(rider.getLastName());
        currentRider.setTelephone(rider.getTelephone());

        riderService.updateRider(currentRider);
        return currentRider == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : new ResponseEntity<Rider>(currentRider, HttpStatus.OK);
    }

    @RequestMapping(value = "/rider/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteRider(@PathVariable("id") Long id) {
        logger.info("Fetching & Deleting Rider with id {}", id);

        Rider rider = riderService.findById(id);
        riderService.deleteRiderById(id);

        return rider == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : new ResponseEntity<Rider>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/rider/", method = RequestMethod.DELETE)
    public ResponseEntity<Rider> deleteAllRiders() {
        riderService.deleteAllRiders();

        return new ResponseEntity<Rider>(HttpStatus.NO_CONTENT);
    }

}
