package wayscout.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import wayscout.model.Party;
import wayscout.model.Ride;
import wayscout.model.RideDetail;
import wayscout.model.Rider;
import wayscout.service.PartyService;
import wayscout.service.RideDetailService;
import wayscout.service.RideService;
import wayscout.service.RiderService;

@RestController
@RequestMapping("/api")
public class RideController {

    public static final Logger logger = LoggerFactory.getLogger(RideController.class);

    @Autowired
    RideService rideService;

    @Autowired
    RideDetailService rideDetailService;

    @Autowired
    RiderService riderService;

    @Autowired
    PartyService partyService;

    @RequestMapping(value = "/ride/", method = RequestMethod.GET)
    public ResponseEntity<List<Ride>> listAllRides() {
        List<Ride> rides = rideService.findAllRides();

        return rides.isEmpty() ? new ResponseEntity(HttpStatus.NO_CONTENT) : new ResponseEntity<List<Ride>>(rides, HttpStatus.OK);
    }

    @RequestMapping(value = "/ride/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getRide(@PathVariable("id") Long id) {
        Ride ride = rideService.findById(id);

        return ride == null ? new ResponseEntity(HttpStatus.NO_CONTENT) : new ResponseEntity<Ride>(ride, HttpStatus.OK);
    }

    @RequestMapping(value = "/ride/", method = RequestMethod.POST)
    public ResponseEntity<?> createRide(@RequestBody Ride ride, UriComponentsBuilder ucBuilder) {
        Set<Rider> riders  = new HashSet<>();
        Set<Party> parties = new HashSet<>();

        for (Object rider : ride.getRiders()) {
            Rider currentRider = riderService.findById(new Long((int) rider));
            riders.add(currentRider);
        }

        for (Object party : ride.getParties()) {
            Party currentParty = partyService.findById(new Long((int) party));
            parties.add(currentParty);
        }

        ride.setRiders(riders);
        ride.setParties(parties);
        rideService.saveRide(ride);

        for (Rider rider : ride.getRiders()) {
            RideDetail rideDetail = new RideDetail();

            // TODO: This info needs to come from the UI
            rideDetail.setDistance(1000);
            rideDetail.setDuration(2000);

            rideDetail.setRideId(ride.getId());
            rideDetail.setRiderId(rider.getId());

            rideDetailService.saveRideDetail(rideDetail);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/ride/").buildAndExpand().toUri());
        return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/ride/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateRide(@PathVariable("id") Long id, @RequestBody Ride ride) {
        Set<Rider> riders = new HashSet<>();
        Set<Party> parties = new HashSet<>();

        for (Object rider : ride.getRiders()) {
            Rider currentRider = riderService.findById(new Long((int) rider));
            riders.add(currentRider);
        }

        for (Object party : ride.getParties()) {
            Party currentParty = partyService.findById(new Long((int) party));
            parties.add(currentParty);
        }

        Ride currentRide = rideService.findById(id);

        currentRide.setRiders(riders);
        currentRide.setParties(parties);
        currentRide.setName(ride.getName());
        currentRide.setTotalDistance(ride.getTotalDistance());
        currentRide.setTotalDuration(ride.getTotalDuration());

        rideService.updateRide(currentRide);
        return currentRide == null ? new ResponseEntity(HttpStatus.NO_CONTENT) : new ResponseEntity<Ride>(currentRide, HttpStatus.OK);
    }

    @RequestMapping(value = "/ride/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteRide(@PathVariable("id") Long id) {
        Ride ride = rideService.findById(id);

        rideService.deleteRideById(id);
        return new ResponseEntity<Ride>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/ride/", method = RequestMethod.DELETE)
    public ResponseEntity<Ride> deleteAllRides() {
        rideService.deleteAllRides();
        return new ResponseEntity<Ride>(HttpStatus.NO_CONTENT);
    }

}

