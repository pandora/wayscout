package wayscout.controller;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import wayscout.model.Party;
import wayscout.model.Ride;
import wayscout.model.Rider;
import wayscout.util.DataFieldUtils;

@Controller
public class AdminController {

    public static final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @GetMapping("/admin/rider")
    public String admin(Model model) {
        String[] columns = new String[] {"", "Name", "Phone", "Actions"};

        DataFieldUtils dataUtils = new DataFieldUtils(Rider.class);

        model.addAttribute("type", "rider");
        model.addAttribute("name", "Manage Riders");
        model.addAttribute("columns", Arrays.asList(columns));
        model.addAttribute("fieldsMap", dataUtils.getFieldsMap());

        return "admin";
    }

    @GetMapping("/admin/party")
    public String party(Model model) {
        String[] columns = new String[] {"", "Name", "Actions"};

        DataFieldUtils dataUtils = new DataFieldUtils(Party.class);

        model.addAttribute("type", "party");
        model.addAttribute("name", "Manage Parties");
        model.addAttribute("columns", Arrays.asList(columns));
        model.addAttribute("fieldsMap", dataUtils.getFieldsMap());

        return "admin";
    }

    @GetMapping("/admin/ride")
    public String ride(Model model) {
        String[] columns = new String[] {"", "Name", "TotalDistance", "TotalDuration", "Actions"};

        DataFieldUtils dataUtils = new DataFieldUtils(Ride.class);

        model.addAttribute("type", "ride");
        model.addAttribute("name", "Manage Rides");
        model.addAttribute("columns", Arrays.asList(columns));
        model.addAttribute("fieldsMap", dataUtils.getFieldsMap());

        return "admin";
    }

}
