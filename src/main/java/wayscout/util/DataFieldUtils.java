package wayscout.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DataFieldUtils {

    private List<String> fields;
    private List<String> excludedFields = Arrays.asList( new String[] {"id", "created", "updated"});

    private String capitalise(String candidate) {
        return StringUtils.capitalize(StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(candidate), StringUtils.SPACE));
    }

    public DataFieldUtils(Class cls) {
        fields = FieldUtils.getAllFieldsList(cls)
                .stream()
                .map(x -> x.getName())
                .filter(f -> !excludedFields.contains(f))
                .collect(Collectors.toList());
    }

    public List<String> getFields() {
        return fields;
    }

    public List<String> getCapitalisedFields() {
        return getFields().stream().map(field -> capitalise(field)).collect(Collectors.toList());
    }

    public Map<String, String> getFieldsMap() {
        return IntStream.range(0, getFields().size())
                .boxed()
                .collect(Collectors.toMap(getFields()::get, getCapitalisedFields()::get));
    }

}
