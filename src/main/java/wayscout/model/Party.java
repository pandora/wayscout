package wayscout.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Party {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @SequenceGenerator(name="party_generator", sequenceName = "party_seq", allocationSize=50)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @NotEmpty
    @Column(name="name", nullable=false)
    private String name;

    @Column(name="created", nullable=false)
    private Date created;

    @Column(name="updated")
    private Date updated;

    @ManyToMany
    @JoinTable(
        name = "party_riders",
        joinColumns = @JoinColumn(name = "party_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "rider_id", referencedColumnName = "id")
    )
    private Set<Rider> riders = new HashSet<Rider>();

    public Set<Rider> getRiders() { return riders; }

    public void setRiders(Set riders) { this.riders = riders; }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }
}
