package wayscout.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Ride {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @SequenceGenerator(name="ride_generator", sequenceName = "ride_seq", allocationSize=50)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @NotEmpty
    @Column(name="name", nullable=false)
    private String name;

    @NotNull
    @Column(name="total_distance", nullable=false) // metres
    private Integer totalDistance;

    @NotNull
    @Column(name="total_duration", nullable=false) // seconds
    private Integer totalDuration;

    @Column(name="created", nullable=false)
    private Date created;

    @Column(name="updated")
    private Date updated;

    @ManyToMany
    @JoinTable(
        name = "ride_parties",
        joinColumns = @JoinColumn(name = "ride_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "party_id", referencedColumnName = "id")
    )
    private Set<Party> parties = new HashSet<Party>();

    @ManyToMany
    @JoinTable(
            name = "ride_riders",
            joinColumns = @JoinColumn(name = "ride_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "rider_id", referencedColumnName = "id")
    )
    private Set<Rider> riders = new HashSet<Rider>();

    public Set<Party> getParties() { return parties; }

    public void setParties(Set parties) { this.parties = parties; }

    public Set<Rider> getRiders() { return riders; }

    public void setRiders(Set riders) { this.riders = riders; }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Integer totalDistance) {
        this.totalDistance = totalDistance;
    }

    public Integer getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(Integer totalDuration) {
        this.totalDuration = totalDuration;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }
}
