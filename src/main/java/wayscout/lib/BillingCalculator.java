package wayscout.lib;

import wayscout.model.Party;
import wayscout.model.Ride;
import wayscout.model.RideDetail;
import wayscout.model.Rider;

import java.util.*;
import java.util.stream.Collectors;

public class BillingCalculator {

    private Double taxiFare;
    private Double commission;

    private Ride ride;
    private List<RideDetail> rideDetails;

    private Double totalCost;

    public BillingCalculator(Double taxiFare, Double commission, Ride ride, List<RideDetail> rideDetails) {
        this.taxiFare   = taxiFare;
        this.commission = commission;
        this.ride       = ride;
        this.rideDetails = rideDetails;

        totalCost = (1 + commission) * taxiFare;
    }

    public Map<Rider, Double> calculatePassengerCosts() {
        Map<Rider, Double> costBreakdown = new HashMap<>();

        // Ultimately, a set of all riders not in a party, once parties are traversed below
        Set <Rider> nonPartyRiders = ride.getRiders();

        Set<Party> parties = ride.getParties();
        Double rideSize    = new Double(ride.getRiders().size());

        Double totalRideDistance = new Double(ride.getTotalDistance());

        for (Party party : parties) {
            // Calculate fare for the party
            Double partySize = new Double(party.getRiders().size());
            System.out.println( partySize);
            System.out.println( rideSize);

            Double partyRideRatio = partySize / rideSize;
            Double partyFare = partyRideRatio * totalCost;

            // Further break down cost for each passenger in the party
            Double costPerPartyPassenger = partyFare / party.getRiders().size();

            for (Rider rider : party.getRiders()) {
                costBreakdown.put(rider, costPerPartyPassenger);

                // Remove riders that are in a party
                nonPartyRiders.remove(rider);
            }
        }

        // Calculate costs for passengers not in any parties
        for (Rider rider : nonPartyRiders) {
            RideDetail riderDetail = getRideDetailByRiderId(rider.getId());

            Double riderDistance = new Double(riderDetail.getDistance());

            // Calculate fare for the individual rider
            Double passengerFare = (riderDistance / totalRideDistance) * totalCost;
            costBreakdown.put(rider, passengerFare);
        }

        return costBreakdown;
    }

    private RideDetail getRideDetailByRiderId(Long riderId) {
        return rideDetails.stream().filter(rd -> rd.getRiderId() == riderId).collect(Collectors.toList()).get(0);
    }

}
