package wayscout;

import org.junit.Before;

import org.junit.Test;
import wayscout.Data.ModelBuilder;
import wayscout.lib.BillingCalculator;
import wayscout.model.Ride;
import wayscout.model.RideDetail;
import wayscout.model.Rider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class ScenarioOneTest extends BaseTest {

    private Rider rider;
    private Ride ride;
    private List<RideDetail> rideDetails = new ArrayList<>();

    @Before
    public void setup() {
        ModelBuilder.setRiderService(riderService);

        rider = ModelBuilder.buildRider("Jon", "Doe", "1232456789");
        ride  = ModelBuilder.buildRide("MyRide", 2000, 3000, rider);

        Long riderId = ride.getRiders().iterator().next().getId();
        rideDetails.add(ModelBuilder.buildRideDetail(ride.getId(), riderId, 2000, 3000));
    }

    @Test
    public void calculateCosts() {
        BillingCalculator billingCalculator = new BillingCalculator(10.00, 0.15, ride, rideDetails);
        Map<Rider, Double> costs = billingCalculator.calculatePassengerCosts();

        assertEquals(1, costs.size());

        Map<Rider, Double> check = new HashMap<>();
        check.put(ride.getRiders().iterator().next(), 11.50);
        assert(costs.equals(check));
    }
}
