package wayscout;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import wayscout.util.ApiRequest;
import wayscout.model.Rider;
import wayscout.Data.ModelBuilder;

import static org.junit.Assert.*;

public class RiderIntegrationTest extends BaseTest {

    //@Test
    public void addAndGet() {
        ApiRequest request = new ApiRequest(restTemplate, port, "/api/rider/");

        ModelBuilder.setRiderService(riderService);
        Rider rider = ModelBuilder.buildRider("Jon", "Doe", "1232456789");

        ResponseEntity<Rider> response = request.postRequest(rider);

        assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);

        ResponseEntity<Rider[]> res = request.getRequest(new Rider[0]);

        assertEquals(res.getStatusCode(), HttpStatus.OK);
        assertEquals("Jon", res.getBody()[0].getFirstName());
    }
}



