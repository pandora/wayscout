package wayscout.util;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import wayscout.Application;
import wayscout.model.Rider;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class ApiRequest {

    @Autowired
    private TestRestTemplate restTemplate;

    private Integer port;

    private String  address = "127.0.0.1";
    private String  endpoint;

    public ApiRequest() {

    }

    public ApiRequest(TestRestTemplate restTemplate, Integer port, String endpoint) {
        this.port = port;
        this.restTemplate = restTemplate;
        this.endpoint = endpoint;
    }

    public <T> ResponseEntity<T> postRequest(T record) {
        return (ResponseEntity<T>) restTemplate.postForEntity(serviceUrl(), new HttpEntity<>(record), record.getClass());
    }

    public <T> ResponseEntity<T> getRequest(T record) {
        return (ResponseEntity<T>) restTemplate.getForEntity(serviceUrl(), record.getClass());
    }

    protected String serviceUrl() {
        return "http://" + address + ":" + port + endpoint;
    }

}
