package wayscout.Data;

import wayscout.model.Party;
import wayscout.model.Ride;
import wayscout.model.RideDetail;
import wayscout.model.Rider;
import wayscout.service.RiderService;

import java.util.HashSet;
import java.util.Set;

public class ModelBuilder {

    private static RiderService riderService;

    public static void setRiderService(RiderService rs) {
        riderService = rs;
    }

    public static Rider buildRider(String firstName, String lastName, String telephone) {
        Rider rider = new Rider();

        rider.setFirstName(firstName);
        rider.setLastName(lastName);
        rider.setTelephone(telephone);

        riderService.saveRider(rider);

        return rider;
    }

    public static Party buildParty(String name, Set<Rider> riders) {
        Party party = new Party();

        party.setName(name);
        party.setRiders(riders);

        return party;
    }

    public static Party buildParty(String name, Rider r) {
        Set<Rider> riders = new HashSet<>();
        riders.add(r);

        return buildParty(name, riders);
    }

    public static Ride buildRide(String name, Integer distance, Integer duration, Set<Party> parties, Set<Rider> riders) {
        Ride ride = new Ride();

        ride.setName(name);
        ride.setTotalDistance(distance);
        ride.setTotalDuration(duration);
        ride.setRiders(riders);
        ride.setParties(parties);

        return ride;
    }

    public static Ride buildRide(String name, Integer distance, Integer duration, Set<Rider> riders) {
        Ride ride = new Ride();

        ride.setName(name);
        ride.setTotalDistance(distance);
        ride.setTotalDuration(duration);
        ride.setRiders(riders);

        return ride;
    }

    public static Ride buildRide(String name, Integer distance, Integer duration, Rider rider) {
        Set<Rider> riders = new HashSet();
        riders.add(rider);

        return buildRide(name, distance, duration, riders);
    }

    public static Ride buildRide(String name, Integer distance, Integer duration, Party party, Rider rider) {
        Set<Rider> riders = new HashSet();
        riders.add(rider);

        Set<Party> parties = new HashSet();
        parties.add(party);

        return buildRide(name, distance, duration, parties, riders);
    }

    public static Ride buildRide() {
        Rider rider = ModelBuilder.buildRider("Jon", "Doe", "1232456789");

        Set<Rider> riders = new HashSet();
        riders.add(rider);

        Party party = ModelBuilder.buildParty("MyParty", riders);
        Set<Party> parties = new HashSet();
        parties.add(party);

        return ModelBuilder.buildRide("MyRide", 2000, 3000, parties, riders);
    }

    public static RideDetail buildRideDetail(Long rideId, Long riderId, Integer distance, Integer duration) {
        RideDetail rideDetail = new RideDetail();

        rideDetail.setRideId(rideId);
        rideDetail.setRiderId(riderId);
        rideDetail.setDistance(distance);
        rideDetail.setDuration(duration);

        return rideDetail;
    }
}
