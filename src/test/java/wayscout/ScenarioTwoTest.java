package wayscout;

import org.junit.Before;
import org.junit.Test;

import wayscout.Data.ModelBuilder;
import wayscout.lib.BillingCalculator;
import wayscout.model.Ride;
import wayscout.model.RideDetail;
import wayscout.model.Rider;

import java.util.*;

import static org.junit.Assert.*;

public class ScenarioTwoTest extends BaseTest {

    private Rider lucy, rebecca;
    private Ride ride;
    private List<RideDetail> rideDetails = new ArrayList<>();

    @Before
    public void setup() {
        ModelBuilder.setRiderService(riderService);

        lucy    = ModelBuilder.buildRider("Lucy", "Lucy", "1232456789");
        rebecca = ModelBuilder.buildRider("Rebecca", "Rebecca", "987654321");

        ride = ModelBuilder.buildRide( "MyRide", 2000, 3000, new HashSet<Rider>(Arrays.asList(lucy, rebecca)) );

        ride.getRiders().forEach(r -> rideDetails.add(ModelBuilder.buildRideDetail(ride.getId(), r.getId(), 1000, 1500)));
    }

    @Test
    public void calculateCosts() {
        BillingCalculator billingCalculator = new BillingCalculator(10.00, 0.20, ride, rideDetails);
        Map<Rider, Double> costs = billingCalculator.calculatePassengerCosts();

        assertEquals(2, costs.size());

        assertEquals((Double) 6.00, (Double) costs.get(lucy));
        assertEquals((Double) 6.00, (Double) costs.get(rebecca));
    }
}
