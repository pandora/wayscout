package wayscout;

import org.junit.Before;
import org.junit.Test;

import wayscout.Data.ModelBuilder;
import wayscout.lib.BillingCalculator;
import wayscout.model.Party;
import wayscout.model.Ride;
import wayscout.model.RideDetail;
import wayscout.model.Rider;

import java.util.*;

import static org.junit.Assert.*;

public class ScenarioThreeTest extends BaseTest {

    private Rider aaron, terry, ben;
    private Party party1, party2;
    private Ride ride;
    private List<RideDetail> rideDetails = new ArrayList<>();

    @Before
    public void setup() {
        aaron = ModelBuilder.buildRider("Aaron", "Aaron", "1232456789");
        terry = ModelBuilder.buildRider("Terry", "Terry", "9999999999");
        ben   = ModelBuilder.buildRider("Ben",   "Ben",   "8888888888");

        party1 = ModelBuilder.buildParty("Party1", aaron);
        party2 = ModelBuilder.buildParty("Party2", new HashSet<Rider>(Arrays.asList(terry, ben)));

        ride = ModelBuilder.buildRide(
            "MyRide",
            3000,
            3000,
            new HashSet<Party>(Arrays.asList(party1, party2)),
            new HashSet<Rider>(Arrays.asList(aaron, terry, ben))
        );

        party1.getRiders().forEach(r -> rideDetails.add(ModelBuilder.buildRideDetail(ride.getId(), r.getId(), 1000, 1000)));
        party2.getRiders().forEach(r -> rideDetails.add(ModelBuilder.buildRideDetail(ride.getId(), r.getId(), 1000, 1000)));
    }

    @Test
    public void calculateCosts() {
        BillingCalculator billingCalculator = new BillingCalculator(60.00, 0.25, ride, rideDetails);
        Map<Rider, Double> costs = billingCalculator.calculatePassengerCosts();

        assertEquals(3, costs.size());

        assertEquals((Double) 25.00, (Double) costs.get(aaron));
        assertEquals((Double) 25.00, (Double) costs.get(terry));
        assertEquals((Double) 25.00, (Double) costs.get(ben));
    }
}
