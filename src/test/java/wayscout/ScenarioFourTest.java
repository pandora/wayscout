package wayscout;

import org.junit.Before;
import org.junit.Test;

import wayscout.Data.ModelBuilder;
import wayscout.lib.BillingCalculator;
import wayscout.model.Ride;
import wayscout.model.RideDetail;
import wayscout.model.Rider;

import java.util.*;

import static org.junit.Assert.*;

public class ScenarioFourTest extends BaseTest {

    private Rider sam, jason;
    private Ride ride;
    private List<RideDetail> rideDetails = new ArrayList<>();

    @Before
    public void setup() {
        ModelBuilder.setRiderService(riderService);

        sam   = ModelBuilder.buildRider("Sam", "Sam", "1232456789");
        jason = ModelBuilder.buildRider("Jason", "Jason", "987654321");
        ride = ModelBuilder.buildRide( "MyRide", 2000, 3000, new HashSet<Rider>(Arrays.asList(sam, jason)) );

        rideDetails.add(ModelBuilder.buildRideDetail(ride.getId(), sam.getId(),  1500, 1500));
        rideDetails.add(ModelBuilder.buildRideDetail(ride.getId(), jason.getId(), 500,  750));
    }

    @Test
    public void calculateCosts() {
        BillingCalculator billingCalculator = new BillingCalculator(40.00, 0.30, ride, rideDetails);
        Map<Rider, Double> costs = billingCalculator.calculatePassengerCosts();

        assertEquals(2, costs.size());

        assertEquals((Double) 39.00, (Double) costs.get(sam));
        assertEquals((Double) 13.00, (Double) costs.get(jason));
    }
}
