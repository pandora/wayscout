## BUSINESS LOGIC

The total Ride distance is assumed to be the sum of all rider distances, regardless of destination.

All riders in a party start and end their journeys together.

Distance could, potentially, also be used to, alternatively, calculate Party costs.

An additional use-case exists where a Ride consists of Parties and Individual Riders.

## ASSUMPTIONS

A MySQL instance exists with an endpoint that the app can connect to. To run MySQL locally with docker, please see instructions below

A Database named _waypoint_ exists.

Passwords are insecure and stored on the filesystem. Ideally they should be stored in the environment, see [12 Factor App](https://12factor.net)

[Thymeleaf](https://www.thymeleaf.org) is used as a template engine to render the views.

### UI & TECHNICAL ASSUMPTIONS

Pagination is not required

Auth is not necessary

The business logic is not multithreaded

The UI needs better validation and error handling capability

## SOURCES

[TutorialRepublic](www.tutorialrepublic.com) was used to source form html/css,
which was later modulerised and had JS added to it.

[Spring Documentation](https://spring.io/docs)

## INSTALLATION

### Requirements

Maven v3.5+

Java 1.8+

### Running MySQL with Docker (OSX)

Pull MySQL: `docker pull mysql`

Run MySQL: `docker-compose run --service-ports -d db`

Check if container is running: `sudo docker ps`

Log into container: `docker exec -it ${container-name} bash`

### RUNNING TESTS

Clone the app: `git clone https://pandora@bitbucket.org/pandora/wayscout.git`

`cd wayscout`

`mvn test`

### RUNNING THE APP

In the home of the checked out source code, run: `mvn spring-boot:run`

Admin Screen are accessible at:

[http://127.0.0.1:8080/admin/rider](http://127.0.0.1:8080/admin/rider)

[http://127.0.0.1:8080/admin/party](http://127.0.0.1:8080/admin/party)

[http://127.0.0.1:8080/admin/ride](http://127.0.0.1:8080/admin/ride)